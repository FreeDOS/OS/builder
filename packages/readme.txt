Put custom packages in here.
They need to be in group subdirectories.

For User Assistance packages that get automatically included, plase those in
a group/directory called assist.

Other packages in other groups must be included in a list. However, when they
are, the packages here will override those from the repository.