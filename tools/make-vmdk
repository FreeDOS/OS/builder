#!/bin/bash

# FYI, I don't know what formulas is for the different virtual machine platforms
# used to determine what the Sectors/Head/Cylinder counts should be when
# creating an image file. The algorithms used in this script seem to (at least)
# occasionally generate the same values. I'm not going to devote any more
# effort into deciphering their behavior at present.

function random () {

    # This is not an encryption safe number randomizer!!!

    local hex='0123456789abcdef'
    local ret=''
    local i
    while [[ ${#ret} -lt ${1} ]] ; do
        i=$(( ( $RANDOM + 1 ) / 2048 - 1 ))
        ret="${ret}${hex:i:1}"
    done
    echo $ret

}

function filesize () {

    local os="$(uname)"
    local ret
    if [[ "$os" == "Darwin" ]] ; then
        ret=$(stat -f %z "${1}" 2>/dev/null)
    elif [[ "$os" == "Linux" ]] ; then
        ret=$(stat --format %s "${1}" 2>/dev/null)
    else
        echo Unsupported platform \"$os\" for stat. >&2
        ret=''
    fi
    echo $ret
    [[ "$ret" != '' ]] && return 0 || return 1

}

function info () {

    local size=$(filesize "${1}")

    local sector_size=512
    local rw_size=$(( ${size} / ${sector_size} ))

    while [[ true ]] ; do
        local x_size=$(( ${size} / ${sector_size} ))
        local sectors=63
        local heads=16
        local cylinders=$(( ${x_size} / ( ${sectors} * ${heads}) ))

        if [[ ${cylinders} -gt 32767 ]] ; then
            sector_size=$(( $sector_size * 2 ))
        else
            break
        fi
    done

    local bsectors=63
    local bheads=2
    local bcylinders=1
    while [[ $(( ${bheads} * ( ${bcylinders} + 1 ) * ${bsectors}  )) -lt ${rw_size} ]] ; do
        (( bcylinders++ ))
        if [[ ${bcylinders} -gt 1024 ]] ; then
            [[ ${bheads} -eq 255 ]] && break
            bcylinders=1
            bheads=$(( ${bheads} * 2 ))
            [[ ${bheads} -gt 255 ]] && bheads=255
        fi
    done
    [[ ${bcylinders} -gt 1024 ]] && bcylinders=1024

    echo "# Disk Descriptor File"
    echo "version=1"
    echo "CID=$(random 8)"
    echo "parentCID=ffffffff"
    echo "createType=\"monolithicFlat\""
    echo
    echo "# Extent description $sector_size byte sectors"
    echo "RW ${rw_size} FLAT \"${1##*/}\" 0"
    echo
    echo "# The Disk Data Base "
    echo "ddb.virtualHWVersion = \"4\""
    echo "ddb.adapterType=\"ide\""
#   echo "ddb.uuid.image=\"\""
#   echo "ddb.uuid.modification=\"\""
    echo "ddb.uuid.parent=\"00000000-0000-0000-0000-000000000000\""
    echo "ddb.uuid.parentmodification=\"00000000-0000-0000-0000-000000000000\""
    echo "ddb.geometry.cylinders=\"${cylinders}\""
    echo "ddb.geometry.heads=\"${heads}\""
    echo "ddb.geometry.sectors=\"${sectors}\""
    echo "ddb.geometry.biosCylinders=\"${bcylinders}\""
    echo "ddb.geometry.biosHeads=\"${bheads}\""
    echo "ddb.geometry.biosSectors=\"${bsectors}\""

}


function vmdk () {

    if [[ "${quite}" == "yes" ]] ; then
        info "${1}" | grep -i "^CID="
    else
        info "${1}"
    fi
    local ext='vmdk'
    [[ "${1##*.}" == "IMG" ]] && ext=VMDK
    if [[ "${1}" != "${1%.*}.${ext}" ]] ; then
        info "${1}" >"${1%.*}.${ext}"
    else
        echo Image should not use vmdk extension. Not saved.
        return 1
    fi

}

[[ "${1}" == "quite" ]] && {
    quite=yes
    shift
}
[[ "${verbose}" != 'yes' ]] && quite=yes

vmdk "${1}" || die

exit 0