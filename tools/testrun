#!/bin/bash

[[ ! ${tools} ]] && export tools=tools

. ${tools}/common

QEMU_RAM=8M
QEMU_HD=500M
QEMU_KEEP=yes
QEMU_BUG=

function help () {
	[[ $# -eq 0 ]] && help -h
	local idx=0
	local media=$(strAppend "${MEDIA_FDI}" "${MEDIA_X86}")
	while [[ ${idx} -lt $# ]] ; do
		(( idx++ ))
		[[ "${!idx}" != '-h' ]] && continue
		echo "usage: ${0##*/} [options] [media]"
		echo 
		echo "	-r size		Virtual machine ram size (default $QEMU_RAM)"
		echo "	-d size		Virtual machine hard disk size (default $QEMU_HD)"
		echo "	-c 		Create new blank hard drive image (default keep, resets)"
		echo 
		echo "	[media]		Media type to test (${media})"
		echo
		echo "note: testing is done through by launching an install inside a QEMU virtual machine"
		return 1
	done
	return 0
}

function strAppend () {
	local previous="${1}"
	shift	
	local seperator=', '
	if [[ ${#} -gt 1 ]] ; then
		separator="${1}"
		shift
	fi
	if [[ "${previous}" == "" ]] ; then
		echo "${@}"
	else
		echo "${previous}${seperator}${@}"
	fi
	return 0
}

function media_types () {
	MEDIA_FDI=
	MEDIA_X86=
	[[ -f ${cache}/fd-floppy.img ]] && MEDIA_FDI=$(strAppend "${MEDIA_FDI}" "floppy")
	[[ -f ${cache}/fd-lite.img ]] &&   MEDIA_FDI=$(strAppend "${MEDIA_FDI}" "lite")
	[[ -f ${cache}/fd-full.img ]] &&   MEDIA_FDI=$(strAppend "${MEDIA_FDI}" "full")
	[[ -f ${cache}/fd-legacy.iso ]] && MEDIA_FDI=$(strAppend "${MEDIA_FDI}" "legacy")
	[[ -f ${cache}/fd-hydra.iso ]] &&  MEDIA_FDI=$(strAppend "${MEDIA_FDI}" "live")
	[[ -f ${www}/${cfg_prefix}-F720k.zip ]] && MEDIA_X86=$(strAppend "${MEDIA_X86}" "720")
	[[ -f ${www}/${cfg_prefix}-F120m.zip ]] && MEDIA_X86=$(strAppend "${MEDIA_X86}" "1.2")
	[[ -f ${www}/${cfg_prefix}-F144m.zip ]] && MEDIA_X86=$(strAppend "${MEDIA_X86}" "1.44")
	[[ -f ${www}/${cfg_prefix}-F288m.zip ]] && MEDIA_X86=$(strAppend "${MEDIA_X86}" "2.88")
	[[ "${MEDIA_X86}" == "${MEDIA_FDI}" ]] && return 1 || return 0
}

function rm_exist () {
	while [[ $# -gt 0 ]] ; do
		if [[ -e "${1}" ]] ; then 
			rm -rf "${1}" || die "could not remove '${1}'"
		fi
		shift
	done
	return 0
}

function cleanup () {
	if [[ "${1}" == '-x' ]] ; then
		rm_exist ${temp}/drive.img
	else
		[[ "${QEMU_KEEP}" != "yes" ]] && rm_exist ${temp}/drive.img
		QEMU_KEEP=yes
	fi
	rm_exist ${temp}/cdrom.iso ${temp}/floppy.img ${temp}/usb.img
}

function test_run () {
	echo "test $QEMU_RAM RAM, $QEMU_HD hard disk (${1})"
	if [[ ! -f ${temp}/drive.img ]] ; then
		qemu-img create -f raw ${temp}/drive.img ${QEMU_HD} || die could not create hard disk image
	fi
	local opts="-m ${QEMU_RAM}"
	opts="${opts} -name FreeDOS"
	# opts="${opts} -no-user-config -nodefaults -rtc base=utc,driftfix=slew -no-hpet"
	opts="${opts} -boot menu=on,strict=on"
	# opts="${opts} -sandbox on,obsolete=deny,elevateprivileges=deny,spawn=deny,resourcecontrol=deny"
	opts="${opts} -msg timestamp=on"
	opts="${opts} -device sb16 -device adlib -soundhw pcspk"
	[[ -e ${temp}/floppy.img ]] && opts="${opts} -drive file=${temp}/floppy.img,media=disk,index=0,if=floppy,format=raw"
	[[ -e ${temp}/usb.img ]] && opts="${opts} -drive file=${temp}/usb.img,media=disk,format=raw"
	[[ -e ${temp}/drive.img ]] && opts="${opts} -drive file=${temp}/drive.img,media=disk,format=raw"
	[[ -e ${temp}/cd.iso ]] && opts="${opts} -cdrom ${temp}/cd.iso"
	# if [[ -e ${temp}/floppy.img ]] ; then
	#	opts="${opts} -boot a'
	#elif [[ -e ${temp}/cd.iso ]] ; then
	#	opts="${opts} -boot d'
	#else
	#	opts="${opts} -boot c'		
	#fi
	local qemu="qemu-system-i386 -curses ${opts}"
	${qemu} || die 
	echo $qemu

}

function test_fdi () {
	echo stage image files for ${1} test
	case ${1} in
		floppy)
			cp -fv ${cache}/fd-floppy.img ${temp}/floppy.img || die staging floppy image	
			cp -fv ${cache}/fd-bonus0.iso ${temp}/cd.iso || die staging cd-rom image	
		;;
		legacy)
			cp -fv ${cache}/fd-legacy.iso ${temp}/cd.iso || die staging cd-rom image	
		;;
		live)
			cp -fv ${cache}/fd-hydra.iso ${temp}/cd.iso || die staging cd-rom image	
		;;
		lite | full)
			cp -fv ${cache}/fd-${1}.img ${temp}/usb.img || die staging cd-rom image	
		;;
	esac
	test_run Standard Edition
		
}

function test_x86 () {
	echo stage image files for ${1} test
	test Floppy Edition test
}

function main () {
	if [[ "${1}" == '--bug' ]] ; then
		QEMU_BUG=yes
		shift
	fi
	if [[ "${1}" == '--dev' ]] ; then
		QEMU_BUG=yes
		shift
		test_run Development Mode
	fi
 	[[ "$QEMU_BUG" == '' ]] && cleanup -x
	while [[ $# -ne 0 ]] ; do
		case ${1} in
			-r)
				QEMU_RAM=${1}
			;;
			-d)
				QEMU_HD=${1}
			;;
			-c) 
				QEMU_KEEP=no
			;;
			*)
				[[ "$QEMU_BUG" == '' ]] && cleanup 
				if [[ "${MEDIA_FDI//${1}}" != "${MEDIA_FDI}" ]] ; then
					test_fdi "${1}"
				elif [[ "${MEDIA_X86//${1}}" != "${MEDIA_X86}" ]] ; then
					test_x86 "${1}"
				else
					die "release media '${1}' not found"
				fi
		esac
		shift
	done
 	[[ "$QEMU_BUG" == '' ]] && cleanup -x
}

media_types

help ${@} || exit 0

main ${@}

exit 0