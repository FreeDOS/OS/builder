# Building a production or unstable interum os version
# unstable=no

# Build release using an alternate branche for projects, tools and packages.
# Leave it unset to build from the latest stable version. Just leave it unset!
# rbe_devel=devel

swd=$(shell echo "${PWD}")

# swd=/home/jerome/rbe

verbose=no
# verbose=yes

shell=/bin/bash

# Directories
cache=cache
tools=tools
temp=temp
mnt=${temp}/mnt
www=www
packages=${cache}/packages
metadata=${cache}/metadata
lists=${cache}/lists

fdi=${cache}/fdi
fdi_x86=${cache}/fdi-x86
fd_nls=${cache}/fd-nls

git_fdi=https://github.com/shidel/FDI.git
git_x86=https://github.com/shidel/FDI-x86.git
git_nls=https://github.com/shidel/fd-nls.git
git_pkg=https://gitlab.com/FreeDOS

today=$(date +"%Y-%m-%d")
stamp=$(date +"%s")

# url pointing to latest version of online repository cdrom iso
cdrom_url=https://www.ibiblio.org/pub/micro/pc-stuff/freedos/files/repositories/1.4/cdrom.iso

# Package repository source, either the new git (using fdvcs.sh) or the old
# cdrom (ibiblio repo cd) method. For fdvcs mode, that script will handle
# downloading and such mundane things.
# repo_mode=cdrom
repo_mode=git

# cdrom_iso should be cache/cdrom.iso or just cdrom.iso
cdrom_iso=${cache}/cdrom.iso

# build process error log
error_log_file=${swd}/${temp}/error.log

# create FDINST partial package from FDNPKG, used for floppy edition
create_fdinst=yes

# settings used for builder hard disk image
builder_size=64M
builder_ram=16M

# setting used to determine size of x86 installer slice archive files
#slice_size=177K
slice_size=59K

# use pass-through compression with slicer
# slice_compress=yes
slice_compress=no

# Release FloppyEdition archive types.
# multiple for all versions are in separate archives
# single for one archive with all versions
floppy_type=single

# Bootable floppy image path+file needed from repo cd-rom
repo_boot=boot.img

# It really slows down the build process.
# So, should only really be used with an actual release.
# Basically, it just provides more accurate file counts and sizes
# in the extracted metadata for programs like FDIMPLES
zip_explode=yes

# filtered metadata fields, prior to update
lsm_filter=';crc;md5;sha;group;begin3;end;'

# include MD5 and SHA256 hash of package in metadata
hash_more=yes

# seconds until a dos task running in a qemu virtual machine is assumed to be
# stuck. if the machine building the release is really really slow, this will
# probably need increased.
# qemu_timeout=150
# qemu_timeout=300
qemu_timeout=1200

# Maximun size (in MB) of a ISO image
max_iso=650

# some things are multi-threaded, this is the Maximum number of concurrent
# processes, 2+ whatever you think your system can handle.
max_threads=10

export >/dev/null

#  Groups for RBE generated packages
group_fdinst=net
group_fdisrc=devel
